Pod::Spec.new do |s|
  s.name         = "LNTXLocalizationKit"
  s.version      = "0.1.0"
  s.summary      = "LNTXLocalizationKit provides a simple and straightforward way to dynamically localise you iOS applications."
  s.homepage     = "http://www.linitix.com"
  s.license      = 'MIT'
  s.author       = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.source       = { :git => "https://bitbucket.org/linitixios/lntxlocalizationkit.git", :tag => '0.1.0' }

  s.platform     = :ios, '6.0'
  s.requires_arc = true
  
  s.ios.exclude_files = '**/*.pch'

  s.source_files = 'LNTXLocalizationKit', 'LNTXLocalizationKit/Categories', 'LNTXLocalizationKit/Private'
end
