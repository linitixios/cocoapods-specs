Pod::Spec.new do |s|
  s.name         = "LNTXAbout"
  s.version      = "0.1.0"
  s.summary      = "LNTXAbout provides view controller that displays information about LINITIX."
  s.homepage     = "http://www.linitix.com"
  s.license      = 'MIT'
  s.author       = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.source       = { :git => "https://bitbucket.org/linitixios/lntxabout.git", :tag => s.version }

  s.platform     = :ios, '6.0'
  s.requires_arc = true
  
  s.ios.exclude_files = '**/*.pch'

  s.source_files = 'LNTXAbout'
  
  s.resources       = "LNTXAbout/*.png", "LNTXAbout/*.storyboard", "LNTXAbout/*.xib"
  s.frameworks      = "Foundation", "UIKit", "CoreGraphics", "MessageUI"
end
