Pod::Spec.new do |s|
  s.name         = "LNTXAbout"
  s.version      = "0.2.1"
  s.summary      = "LNTXAbout provides view controller that displays information about LINITIX."
  s.homepage     = "http://www.linitix.com"
  s.license      = 'MIT'
  s.author       = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.source       = { :git => "https://bitbucket.org/linitixios/lntxabout.git", :tag => '0.2.1' }

  s.platform     = :ios, '6.0'
  s.requires_arc = true
  
  s.ios.exclude_files = '**/*.pch'

  s.source_files = 'LNTXAbout/*.{h,m}'
  
  s.resources       = "LNTXAbout/*.png", "LNTXAbout/*.storyboard", "LNTXAbout/*.xib", "LNTXAbout/*.lproj"
  s.frameworks      = "Foundation", "UIKit", "CoreGraphics", "MessageUI"
  s.dependency 'LNTXLocalizationKit', '~> 0.1'
end
