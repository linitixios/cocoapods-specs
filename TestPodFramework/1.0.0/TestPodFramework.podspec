Pod::Spec.new do |s|
  s.name         = "TestPodFramework"
  s.version      = "1.0.0"
  s.summary      = "Testing Pod Frameworks."

  s.description  = <<-DESC
                   Testing testing.
                   DESC

  s.homepage     = "http://www.linitix.com"

  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  
  s.author             = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.social_media_url   = "http://twitter.com/DamienRambout"

  s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:linitixios/testpodframework.git", :tag => s.version }

  s.source_files  = "TestPodFramework"
  s.exclude_files = '**/*.pch'

  s.resources = "TestPodFramework/**/*.plist"

  s.module_name = "TestPodFramework"

  s.requires_arc = true
end
