Pod::Spec.new do |s|
  s.name         = "Trakt2"
  s.version      = "0.1.4"
  s.summary      = "An Objc wrapper library that communicates with Trakt API v2."

  s.description  = <<-DESC
                   A library that communicates with Trakt API v2, using standard Objective-C objects.
                   DESC

  s.homepage     = "http://www.linitix.com"

  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  
  s.author             = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.social_media_url   = "http://twitter.com/DamienRambout"

  s.platform     = :ios, "7.0"

  s.source       = { :git => "git@bitbucket.org:linitixios/trakt2.git", :tag => s.version }

  s.source_files  = "Trakt2"
  s.exclude_files = '**/*.pch'

  s.resources = "Trakt2/Resources/*.plist"
  s.preserve_paths = "Trakt2/Resources"

  s.requires_arc = true

  s.dependency "LNTXWebServiceHelper", "~> 0.1"
  s.dependency "NXOAuth2Client", "~> 1.2.6"
end
