Pod::Spec.new do |s|
  s.name         = "LNTXWebServiceHelper"
  s.version      = "0.4.1"
  s.summary      = "LNTXWebServiceHelper provides tools to easily communicate with remote web services."
  s.homepage     = "http://www.linitix.com"
  s.license      = 'MIT'
  s.author       = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.source       = { :git => "https://bitbucket.org/linitixios/lntxwebservicehelper.git", :tag => s.version }

  s.platform     = :ios, '7.0'
  s.requires_arc = true
  
  s.ios.exclude_files = '**/*.pch'

  s.source_files = 'LNTXWebServiceHelper'
  
  s.dependency 'LNTXCoreKit', '~> 0.1'
  s.dependency 'LNTXParser', '~> 0.1'
  s.dependency 'AFNetworking', '~> 2.1'
end
