Pod::Spec.new do |s|
  s.name         = "LNTXCoreKit"
  s.version      = "0.1.0"
  s.summary      = "LNTXCoreKit provides several basic classes and functions that are reusable in any project."
  s.homepage     = "http://www.linitix.com"
  s.license      = 'MIT'
  s.author       = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.source       = { :git => "https://bitbucket.org/linitixios/lntxcorekit.git", :tag => '0.1.0' }

  s.platform     = :ios, '6.0'
  s.requires_arc = true
  
  s.ios.exclude_files = '**/*.pch'

  s.source_files = 'LNTXCoreKit'
end
