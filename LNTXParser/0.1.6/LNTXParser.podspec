Pod::Spec.new do |s|
  s.name         = "LNTXParser"
  s.version      = "0.1.6"
  s.summary      = "LNTXParser provides parsing tools to convert various data formats into data model objects."
  s.homepage     = "http://www.linitix.com"
  s.license      = 'MIT'
  s.author       = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.source       = { :git => "https://bitbucket.org/linitixios/lntxparser.git", :tag => s.version }

  s.platform     = :ios, '6.0'
  s.requires_arc = true
  
  s.ios.exclude_files = '**/*.pch'

  s.source_files = 'LNTXParser'
  s.dependency 'LNTXCoreKit', '~> 0.1'
end
