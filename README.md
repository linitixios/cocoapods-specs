# A repository of LINITIX CocoaPods specifications.

This repository contains specifications of Objective-C libraries, which are used by LINITIX to manage library dependencies in its Xcode project.

Note that not all projects are open-source.


## Using specifications

First of all, you need to install the tools.

```
$ [sudo] gem install cocoapods
```

Add the **LINITIX** repository to you CocoaPods repositories.

```
$ pod repo add LINITIX https://bitbucket.org/linitixios/cocoapods-specs.git
```

Check the repository is correctly installed by running the following command:

```
$ pod repo lint LINITIX
```

You are now ready to use our Pods in your projects.

### Versioning

CocoaPods uses a versioning scheme known as [Semantic Versioning](http://semver.org/). See this [example](https://github.com/CocoaPods/Specs/wiki/Cross-dependencies-resolution-example) for more info on why this scheme is used.

## License

These specifications and CocoaPods are available under the [MIT license](http://www.opensource.org/licenses/mit-license.php).

